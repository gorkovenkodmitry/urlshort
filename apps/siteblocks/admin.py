#coding: utf-8
from django.contrib import admin
from django import forms
from import_export.admin import ImportExportModelAdmin
from mptt.admin import MPTTModelAdmin
from salmonella.admin import SalmonellaMixin
from apps.siteblocks.models import Settings, Menu
from apps.utils.widgets import Redactor, TinyMCE


class SettingsAdminForm(forms.ModelForm):
    class Meta:
        model = Settings
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SettingsAdminForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs['instance']
        except KeyError:
            instance = False
        if instance:
            if instance.type == u'input':
                self.fields['value'].widget = forms.TextInput()
            elif instance.type == u'textarea':
                self.fields['value'].widget = forms.Textarea()
            elif instance.type == u'redactor':
                self.fields['value'].widget = TinyMCE(attrs={'cols': 100, 'rows': 10},)


class SettingsAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('title', 'name', 'value',)
    form = SettingsAdminForm
    # readonly_fields = ('name', 'type',)

admin.site.register(Settings, SettingsAdmin)


class MenuAdmin(ImportExportModelAdmin, SalmonellaMixin, MPTTModelAdmin):
    list_display = ('id', 'name', 'parent', 'alias', 'order', 'is_published', 'target',)
    list_display_links = ('id', 'name', 'parent',)
    list_editable = ('order', 'is_published', 'target', 'alias')
    list_filter = ('is_published', 'target',)
    salmonella_fields = ('parent',)
    mptt_indent_field = 'name'
admin.site.register(Menu, MenuAdmin)