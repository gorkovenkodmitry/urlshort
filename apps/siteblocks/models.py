#coding: utf-8
from django.db import models
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from apps.utils.managers import TreeManager, TreeManagerVisible


type_choices = (
    (u'input', u'input'),
    (u'textarea', u'textarea'),
    (u'redactor', u'redactor'),
)


class Settings(models.Model):
    title = models.CharField(verbose_name=u'Название', max_length=150)
    name = models.CharField(verbose_name=u'Служебное имя', max_length=250, unique=True)
    value = models.TextField(verbose_name=u'Значение')
    type = models.CharField(max_length=20, verbose_name=u'Тип значения', choices=type_choices)

    class Meta:
        verbose_name = u'настройки сайта'
        verbose_name_plural = u'настройки сайта'

    def __unicode__(self):
        return u'%s' % self.name


class Menu(MPTTModel):
    parent = TreeForeignKey('self', verbose_name=u'Родительский Элемент', related_name='children', blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=30, verbose_name=u'Имя')
    alias = models.CharField(max_length=150, verbose_name=u'Ссылка')

    order = models.IntegerField(default=10, verbose_name=u'Порядок сортировки')
    is_published = models.BooleanField(default=True, verbose_name=u'Опубликовать')
    target = models.BooleanField(verbose_name=u'На новой вкладке', default=False)

    objects = TreeManager()
    items = TreeManagerVisible()

    class Meta:
        verbose_name = u'элемент меню'
        verbose_name_plural = u'элементы меню'

    class MPTTMeta:
        order_insertion_by = ['-order']

    def __unicode__(self):
        return self.name

    def get_children(self):
        qs = super(Menu, self).get_children()
        return qs.order_by('-order').filter(is_published=True)

    def get_ancestors_with_self(self):
        return self.get_ancestors(include_self=True)