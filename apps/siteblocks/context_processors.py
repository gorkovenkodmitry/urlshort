#coding: utf-8
from django.contrib.sites.models import Site
from apps.siteblocks.models import Settings, Menu


def settings(request):
    all_settings = Settings.objects.all()
    result = {
        'SITE_NAME': Site.objects.get_current().name
    }
    for el in all_settings:
        result[el.name] = el.value
    result['menu'] = Menu.items.extra(where=['parent_id IS NULL']).order_by('-order')
    return result