#coding: utf-8
from django.http import Http404, HttpResponseRedirect
from django.conf import settings
from apps.shorter.models import Shorter


class ShorterFallbackMiddleware(object):

    def process_response(self, request, response):
        if response.status_code != 404:
            return response
        try:
            try:
                shorter = Shorter.objects.get(key=request.path_info[1:])
            except Shorter.DoesNotExist:
                raise Http404
            return HttpResponseRedirect(shorter.url)
        except Http404:
            return response
        except:
            if settings.DEBUG:
                raise
            return response