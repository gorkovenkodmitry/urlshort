# coding: utf-8
from django.contrib.sites.models import Site
from django.db import models


class Shorter(models.Model):
    date_add = models.DateTimeField(verbose_name=u'Дата добавления', auto_now_add=True)
    date_change = models.DateTimeField(verbose_name=u'Дата обновления', auto_now=True)
    url = models.URLField(verbose_name=u'URL', unique=True)
    key = models.CharField(verbose_name=u'Ключ', max_length=20, unique=True)

    class Meta:
        verbose_name = u'Ссылки'
        verbose_name_plural = u'Ссылки'
        ordering = ['-pk', ]

    def __unicode__(self):
        return self.url

    def get_absolute_url(self):
        site = Site.objects.get_current()
        return u'%s/%s' % (site.domain, self.key)
