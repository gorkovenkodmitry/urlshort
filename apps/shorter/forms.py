# coding: utf-8
from django import forms


class ShorterForm(forms.Form):
    url = forms.URLField()
