from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from apps.shorter.views import ShorterGet

urlpatterns = [
    url(r'^$', csrf_exempt(ShorterGet.as_view()), name='index'),
]
