# coding: utf-8
from django.contrib import admin
from apps.shorter.models import Shorter


class ShorterAdmin(admin.ModelAdmin):
    list_display = ('date_add', 'date_change', 'url', 'key', )
    list_display_links = ('date_add', 'date_change', 'url', 'key', )
admin.site.register(Shorter, ShorterAdmin)
