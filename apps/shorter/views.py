# coding: utf-8
import hashlib
from django.http.response import JsonResponse
from django.views.generic.base import View
from apps.shorter.forms import ShorterForm
from apps.shorter.models import Shorter


class ShorterGet(View):

    def post(self, request, *args, **kwargs):
        form = ShorterForm(request.POST)
        if form.is_valid():
            try:
                shorter = Shorter.objects.get(url=form.cleaned_data.get('url'))
                key = shorter.key
            except Shorter.DoesNotExist:
                key = hashlib.sha256(form.cleaned_data.get('url').encode('utf-8')).hexdigest()[:5]
                ind = 0
                while Shorter.objects.filter(key=key).exists():
                    key = hashlib.sha256((u'%s%s' % (form.cleaned_data.get('url'), ind)).encode('utf-8')).hexdigest()[:8]
                    ind += 1
                shorter = Shorter.objects.create(
                    key=key,
                    url=form.cleaned_data.get('url'),
                )
            result = {
                'status': 'success',
                'url': shorter.get_absolute_url(),
            }
        else:
            result = {
                'status': 'error',
                'url': form.errors['url'][0],
            }
        return JsonResponse(result)
