#coding: utf-8
from django.db import models
from mptt.models import MPTTModel as mptt_MPTTModel, TreeManager as mpttTreeManager


class PublishedMixin(object):
    def published(self):
        return self.filter(is_published=True)


class VisibleObjects(models.Manager):

    def get_queryset(self):
        return super(VisibleObjects, self).get_queryset().filter(is_published=True)


class TreeManager(PublishedMixin, mpttTreeManager):
    def get_queryset(self):
        return super(TreeManager, self).get_queryset().order_by('-' + self.tree_id_attr, self.left_attr)


class TreeManagerVisible(PublishedMixin, mpttTreeManager):
    def get_queryset(self):
        return super(TreeManagerVisible, self).get_queryset().order_by('-'+self.tree_id_attr, self.left_attr).filter(is_published=True)