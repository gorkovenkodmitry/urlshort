from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from views import upload_img, upload_file, FileBrowseView

urlpatterns = [
    #Redactor
    url(r'^upload_img/$', upload_img),
    url(r'^upload_file/$', upload_file),
    url(r'^admin/filebrowser/$', csrf_exempt(FileBrowseView.as_view())),
]
