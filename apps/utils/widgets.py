#coding: utf-8
from django import forms
from django.utils.safestring import mark_safe


class Redactor(forms.Textarea):
    toolbar = u'default'

    class Media:
        js = (
            '/media/js/redactor/jquery.js',
            '/media/js/redactor/redactor.min.js',
        )
        css = {
            'all': ('/media/js/redactor/css/redactor.css',)
        }

    def __init__(self, attrs=None):
        self.attrs = attrs
        if attrs:
            self.attrs.update(attrs)
        super(Redactor, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        rendered = super(Redactor, self).render(name, value, attrs)
        return rendered + mark_safe(u'''<script type="text/javascript">
        $(document).ready(
            function()
            {
                $('#id_%s').redactor({
                    focus: true,
                    toolbar:'%s',
                    imageUpload:'/upload_img/',
                    fileUpload:'/upload_file/',
                    lang:'ru'
                });
            }
        );
        </script>''' % (name, self.toolbar))


class AdminImageWidget(forms.FileInput):
    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append((u'<a target="_blank" href="%s">'
                           u'<img src="%s" style="height: 100px;" /></a>'
                           % (value.url, value.url)))
        output.append(super(AdminImageWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class TinyMCE(forms.Textarea):

    class Media:
        css = {
            'all': (
                '/media/js/libs/jquery.ui/jquery-ui.min.css',
                '/media/js/libs/filebrowser.css',
            )
        }
        js = (
            '/media/js/libs/jquery.min.js',
            '/media/js/libs/jquery.ui/jquery-ui.min.js',
            '/media/js/libs/upload_file.js',
            '/media/js/libs/filebrowser.js',
            '/media/js/tinymce/tinymce.min.js',
        )

    def __init__(self, attrs=None):
        self.attrs = attrs
        if attrs:
            self.attrs.update(attrs)
        super(TinyMCE, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        rendered = super(TinyMCE, self).render(name, value, attrs)
        return rendered + mark_safe(u'''<script type="text/javascript">
            tinymce.init({
                selector: "#id_%s",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback: function(field_name, url, type, win) {
                    file_browser(win, field_name, '/admin/filebrowser/');
                },
            });
            </script>''' % (name))
