#coding: utf-8
import datetime


def custom_processor(request):
    current_url = request.path
    current_date = datetime.datetime.now()
    main_url = current_url.split('/')[1] if len(current_url.split('/')) > 1 else ''
    if current_url.find('catalog') > -1:
        main_url = '/'

    return {
        'current_url': current_url,
        'current_menu': current_url,
        'current_date': current_date,
        'main_url': main_url,
    }