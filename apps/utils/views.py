#coding: utf-8
import json
import os
import datetime
import imghdr
from PIL import Image
from django import http
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from pytils.translit import translify
from hashlib import sha256
from project import settings as base


def handle_uploaded_file(f, filename, folder):
    name, ext = os.path.splitext(translify(filename).replace(' ', '_'))
    hashed_name = sha256(name + datetime.datetime.now().strftime("%Y%m%d%H%M%S")).hexdigest()
    uploads_dir = os.path.join(base.MEDIA_ROOT, 'uploads')
    if not os.path.isdir(uploads_dir):
        os.mkdir(uploads_dir)
    uploads_dir = os.path.join(uploads_dir, folder)
    if not os.path.isdir(uploads_dir):
        os.mkdir(uploads_dir)
    path_name = base.MEDIA_ROOT + '/uploads/' + folder + hashed_name + ext
    destination = open(path_name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    return '/media/uploads/' + folder + hashed_name + ext

@csrf_exempt
def upload_img(request):
    if request.user.is_staff:
        if request.method == 'POST':
            url = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name, 'images/')
            #Resizing
            size = 650, 650
            im = Image.open(base.ROOT_PATH + url)
            imageSize = im.size
            if (imageSize[0] > size[0]) or (imageSize[1] > size[1]):
                im.thumbnail(size, Image.ANTIALIAS)
                im.save(base.ROOT_PATH + url, "JPEG", quality=100)
            return http.HttpResponse('<img src="%s"/>' % url)

        else:
            return http.HttpResponse('error')
    else:
        return http.HttpResponse('403 Forbidden. Authentication Required!')

@csrf_exempt
def upload_file(request):
    if request.user.is_staff:
        if request.method == 'POST':
            url = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name, 'files/')
            url = '<a href="%s" target=_blank>%s</a>' % (url, request.FILES['file'].name)
            return http.HttpResponse(url)
    else:
        return http.HttpResponse('403 Forbidden. Authentication Required!')


class FileBrowseView(View):

    def get(self, request, *args, **kwargs):
        dir_path = os.path.join(base.MEDIA_ROOT, 'file_browse')
        dir_url = u'%sfile_browse/' % base.MEDIA_URL
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        dir_list = []
        images = []
        files = []
        for name in os.listdir(dir_path):
            if os.path.isdir(os.path.join(dir_path, name)):
                dir_list.append(u'%s%s' % (dir_url, name))
                continue
            elif imghdr.what(os.path.join(dir_path, name)):
                images.append(u'%s%s' % (dir_url, name.decode('utf-8')))
            else:
                files.append(u'%s%s' % (dir_url, name))
        return HttpResponse(json.dumps({
            'status': 'success',
            'dir_list': dir_list,
            'images': images,
            'files': files,
            'dir_url': dir_url
        }))

    def post(self, request, *args, **kwargs):
        dir_path = os.path.join(base.MEDIA_ROOT, 'file_browse')
        dir_url = u'%sfile_browse/' % base.MEDIA_URL
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        path_name = os.path.join(dir_path, request.FILES['file_dialog_input'].name)
        if os.path.exists(path_name):
            d = 0
            path_name1 = '%s_%02i.%s' % ('.'.join(path_name.split('.')[:-1]), d, path_name.split('.')[-1])
            while os.path.exists(path_name1):
                d += 1
                path_name1 = '%s_%02i.%s' % ('.'.join(path_name.split('.')[:-1]), d, path_name.split('.')[-1])
            path_name = path_name1
        destination = open(path_name, 'wb+')
        for chunk in request.FILES['file_dialog_input'].chunks():
            destination.write(chunk)
        destination.close()
        return HttpResponse(json.dumps({
            'status': 'success'
        }))
