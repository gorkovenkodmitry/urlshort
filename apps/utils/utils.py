#coding: utf-8
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string


def send_email(email_title, email_template, dictionary, receivers):
    try:
        current_site = Site.objects.get_current()
        subject = u'%s - %s' % (current_site.name, email_title)
        subject = u''.join(subject.splitlines())

        message = render_to_string(email_template, dictionary).encode('utf-8')
        msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, receivers)
        msg.content_subtype = "html"
        msg.send()
        return True
    except:
        return False


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
