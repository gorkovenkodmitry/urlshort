#coding: utf-8
import os
import datetime
from django.db import models
from apps.utils.models import BaseDoc, BasePic


class Page(models.Model):
    date_add = models.DateTimeField(verbose_name=u'Дата создания', default=datetime.datetime.now)
    title = models.CharField(max_length=120, verbose_name=u'Заголовок страницы')
    url = models.CharField(max_length=200, verbose_name=u'Адрес', unique=True, help_text=u'Адрес страницы на латинице. Например, "/your_address/"')
    content = models.TextField(verbose_name=u'Содержимое страницы', blank=True, default=u'')
    order = models.IntegerField(verbose_name=u'Порядок сортировки', default=10)
    is_published = models.BooleanField(verbose_name=u'Опубликовано', default=True)
    template = models.CharField(verbose_name=u'шаблон', max_length=100, default=u'default.html')

    page_title = models.CharField(max_length=100, verbose_name=u'Заголовок', blank=True, default=u'')
    description = models.CharField(max_length=100, verbose_name=u'description', blank=True, default=u'')
    keywords = models.CharField(max_length=100, verbose_name=u'keywords', blank=True, default=u'')

    class Meta:
        verbose_name = u'страница'
        verbose_name_plural = u'страницы'

    def __unicode__(self):
        return u'%s (%s)' % (self.title, self.get_absolute_url())

    def get_absolute_url(self):
        return self.url

    def save(self, **kwargs):
        if not self.url.endswith('/'):
            self.url += '/'
        if not self.url.startswith('/'):
            self.url = "/" + self.url

        # remove the first and the last space
        self.title = self.title.strip()
        super(Page, self).save(**kwargs)


class PageDoc(BaseDoc):
    page = models.ForeignKey(Page, verbose_name=u'страница')

    class Meta:
        verbose_name = u'файл'
        verbose_name_plural = u'файлы'

    def __unicode__(self):
        return u'%s' % self.title

    def get_upload_path(self, filename):
        return os.path.join('files', 'page_docs')


class PagePic(BasePic):
    page = models.ForeignKey(Page, verbose_name=u'страница')

    class Meta:
        verbose_name = u'картинка'
        verbose_name_plural = u'картинки'

    def __unicode__(self):
        return u'%s' % self.title

    def get_upload_path(self, filename):
        return os.path.join('files', 'page_pics')


class MetaData(models.Model):
    url = models.CharField(max_length=100, verbose_name=u'Адрес', help_text=u'Адрес страницы,например, "/your_address/"')
    title = models.CharField(max_length=100, verbose_name=u'Заголовок')
    description = models.CharField(max_length=100, verbose_name=u'description', blank=True)
    keywords = models.CharField(max_length=100, verbose_name=u'keywords', blank=True)

    class Meta:
        verbose_name = u'мета информация'
        verbose_name_plural = u'мета информация'

    def __unicode__(self):
        return u'%s| %s' % (self.url, self.title)

    def save(self, **kwargs):
        if not self.url.endswith('/'):
            self.url += '/'
        if not self.url.startswith('/'):
            self.url = "/" + self.url

        # remove the first and the last space
        self.title = self.title.strip()
        super(MetaData, self).save(**kwargs)

    save.alters_data = True
