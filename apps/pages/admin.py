#coding: utf-8
from django.contrib import admin
from django import forms
from models import Page, MetaData, PageDoc, PagePic
from apps.utils.widgets import Redactor, AdminImageWidget, TinyMCE
from sorl.thumbnail.admin import AdminImageMixin


class PageDocInline(admin.TabularInline):
    model = PageDoc
    sortable_field_name = 'order'
    extra = 0


class PagePicInlineForm(forms.ModelForm):
    file = forms.ImageField(label=u'Картинка', widget=AdminImageWidget)


class PagePicInline(AdminImageMixin, admin.TabularInline):
    model = PagePic
    form = PagePicInlineForm
    sortable_field_name = 'order'
    extra = 0


class PageAdminForm(forms.ModelForm):
    content = forms.CharField(
        widget=TinyMCE(attrs={'cols': 170, 'rows': 20}),
        label=u'Текст',
        required=False
    )

    class Meta:
        model = Page
        fields = '__all__'


class PageAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ('title', 'url', 'order', 'is_published',)
    list_display_links = ('title', 'url',)
    list_editable = ('is_published', 'order',)
    search_fields = ('title', 'url', 'content',)
    fieldsets = (
        (u'Основные поля', {
            'fields': ('date_add', 'url', 'title', 'content', ),
        }),
        (u'Параметры публикации', {
            'fields': ('order', 'is_published', 'template', ),
        }),
        (u'SEO элементы', {
            'classes': ('collapse', ),
            'fields': ('page_title', 'description', 'keywords')
        })
    )
    list_select_related = True
    form = PageAdminForm
    inlines = [
       PageDocInline,
       PagePicInline,
    ]


class MetaDataAdmin(admin.ModelAdmin):
    list_display = ('url', 'title',)
    list_display_links = ('url', 'title',)
    search_fields = ('url', 'title',)

admin.site.register(MetaData, MetaDataAdmin)
admin.site.register(Page, PageAdmin)
