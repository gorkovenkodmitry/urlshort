#coding: utf-8
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader, RequestContext
from apps.pages.models import Page


def page(request, url):
    if not url.endswith('/'):
        url = u'%s/' % url
    if not url.startswith('/'):
        url = "/" + url
    page = get_object_or_404(Page, url__exact=url)
    t = loader.get_template('pages/' + page.template)
    c = RequestContext(request, {'page': page, 'current_menu': page.url})
    return HttpResponse(t.render(c))
