# Сервис по созданию коротких ссылок #

### Зависимости ###
* python-virtualenv
* libmysqlclient-dev
* uwsgi
* supervisor

Сервис располагается по следующему адресу [http://strange1231.fvds.ru/us](http://strange1231.fvds.ru/us)

Доступ в административную часть (для просмотра всех хранящихся ссылок) [http://strange1231.fvds.ru/us/admin/](http://strange1231.fvds.ru/us/admin/) admin:123

Клиентская часть [http://strange1231.fvds.ru/urlshort-client/](http://strange1231.fvds.ru/urlshort-client/)

Клиентской частью выступает обычная html страница, она может быть перемещена в любое место, а также запущена с локальной машины. Все необходимые зависимости и скрипты хранятся в коде страницы.

## Инструкция по развертыванию проекта ##
Сервис требует виртуального окружения (python-virtualenv), все зависимости описаны в файле project/requirements.txt

БД является MySQL (для корректной установки окружения является пакет libmysqlclient-dev)

После установки окружения нужно указать путь к нему в файле project/django.wsgi

Сервис запущен на технологии uwsgi и общается с сервером nginx через сокет (зависимость python-uwsgi)
Для настройки параметров запуска необходимо отредактировать файл uwsgi.ini. Запуск приложения происходит коммандой uwsgi --ini /var/www/urlshort/uwsgi.ini

Для автоматического запуска используется сервис supervisor. Пример файла настройки

```
[program:urlshort]
command=uwsgi --ini /var/www/urlshort/uwsgi.ini
stdout_logfile=/var/log/supervisor/urlshort.uwsgi.log
stderr_logfile=/var/log/supervisor/urlshort.uwsgi_err.log
autostart=true
autorestart=true
redirect_stderr=true

```

Далее необходимо указать настройки nginx для общения с нашим uwsgi приложением.

```
upstream urlshort {
	server unix:///var/www/urlshort/django.sock;
}
server {
	listen 80 default_server;
	listen [::]:80 default_server;
	charset UTF-8;
	client_max_body_size 25M;
	keepalive_timeout 5;
	root /var/www;

	index index.html index.htm index.nginx-debian.html;

	server_name _;

	location / {
		try_files $uri $uri/ =404;
		autoindex on;
	}

	location /us {
		uwsgi_pass urlshort;
		include /var/www/urlshort/uwsgi_params;
		uwsgi_param SCRIPT_NAME /us;
		uwsgi_modifier1 30;
	}

	location /static {
		alias /var/www/urlshort/static;
	}

	location /media {
		alias /var/www/urlshort/media;
	}

}

```
