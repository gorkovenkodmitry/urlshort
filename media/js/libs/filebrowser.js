function get_obj(data) {
    if (typeof(data) != 'object') {
        return jQuery.parseJSON(data);
    } else {
        return data;
    }
}

function file_browser(win, input_id, url) {
    //this.open = function() {
    //    win.document.getElementById(input_id).value = 'my value';
    //}
    //win.document.getElementById(input_id).value = '/static/admin_tools/images/django.png';
    var dialog = $('#file_dialog');
    if (dialog.length == 0) {
        $('body').append('<div id="file_dialog" title="Просмотр файлов"></div>');
        dialog = $('#file_dialog');
    }
    dialog.dialog({
        autoOpen: false,
        modal: true,
        width: 800,
        height: 600
    });
    dialog.dialog('open');
    function reload() {
        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                var obj = get_obj(data);
                dialog.html('');
                dialog.append('<h3>Текущая папка: '+obj.dir_url+'</h3>');
                dialog.append('<form enctype="multipart/form-data" action="'+url+'" method="post"><p>Загрузить: <input type="file" name="file_dialog_input" /></p></form>');
                dialog.append('<ul class="file_dialog_list"></ul>');
                var i;
                for (i=0; i<obj.dir_list.length; i++) {
                    dialog.find('.file_dialog_list').append('<li class="dir" data-val="'+obj.dir_list[i]+'"><img src="/media/img/folder.png" width="64" height="64" /><span>'+obj.dir_list[i].split('/')[obj.dir_list[i].split('/').length-1]+'</span></li>');
                }
                for (i=0; i<obj.images.length; i++) {
                    dialog.find('.file_dialog_list').append('<li class="img" data-val="'+obj.images[i]+'"><img src="'+obj.images[i]+'" width="64" height="64" /><span>'+obj.images[i].split('/')[obj.images[i].split('/').length-1]+'</span></li>');
                }
                for (i=0; i<obj.files.length; i++) {
                    dialog.find('.file_dialog_list').append('<li class="file" data-val="'+obj.files[i]+'"><img src="/media/img/file_blank.png" width="64" height="64" /><span>'+obj.files[i].split('/')[obj.files[i].split('/').length-1]+'</span></li>');
                }
            }
        });
    }
    reload();
    $(dialog).on('dblclick', '.img, .file', function() {
        $('#'+input_id).val($(this).attr('data-val'));
        dialog.dialog('close');
    });
    $(dialog).on('change', 'input[name="file_dialog_input"]', function(e) {
        var form = dialog.find('form');
        form.ajaxForm(function() {
            reload();
        });
        form.ajaxSubmit({
            url: form.attr('action'),
            success: function(data, status, xhr) {
                reload();
            }
        });
    });
}