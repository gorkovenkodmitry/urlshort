/**
 * Created by strange on 23.11.15.
 */
function GreetCtrl($scope) {
    $scope.name = 'World';
}

function ListCtrl($scope) {
    $scope.names = ['Igor', 'Misko', 'Vojta'];
}

function MyCtrl($scope) {
    $scope.action = function() {
        $scope.name = 'OK';
    };

    $scope.name = 'World';
}

angular.module('directive', []).directive('contenteditable', function() {
    return {
    };
});

function Ctrl1($scope) {
    $scope.name = 'angular';
}

function Ctrl2($scope) {
  $scope.format = 'M/d/yy h:mm:ss a';
}

angular.module('time', []).directive('myCurrentTime', function($timeout, dateFilter) {
    // возвращаем функцию линковки директивы. (компилировать функцию не нужно)
    return function(scope, element, attrs) {
        var format,  // формат даты
        timeoutId; // timeoutId, так что мы можем останавливать обновление времени

        // используя обновление UI
        function updateTime() {
            element.text(dateFilter(new Date(), format));
        }

        // проверка выражения и обновление UI при изменении.
        scope.$watch(attrs.myCurrentTime, function(value) {
            format = value;
            updateTime();
        });

        // расписание обновлений за секунду
        function updateLater() {
            // сохраняем timeoutId для отмены
            timeoutId = $timeout(function() {
                updateTime(); // обновляем DOM
                updateLater(); // расписание других обновлений
            }, 1000);
        }

        // прослушиваем разрушающее (removal) DOM событие, и отменяем следующее обновление UI
        // для предотвращения обновления времени после того как DOM-элемент был удален.
        element.bind('$destroy', function() {
            $timeout.cancel(timeoutId);
        });

        updateLater(); // убиваем процесс обновления UI.
    }
});