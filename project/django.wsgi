"""
WSGI config for child_terr project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys
virtual_env = '/var/www/env/urlshort/bin/activate_this.py'
execfile(virtual_env, dict(__file__=virtual_env))
ROOT_PATH = os.path.dirname(os.path.dirname(__file__))

if not ROOT_PATH in sys.path:
    sys.path.insert(0, ROOT_PATH)

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")

application = get_wsgi_application()
