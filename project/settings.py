#coding: utf-8
import os

ROOT_PATH = os.path.dirname(os.path.dirname(__file__))

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}
TIME_ZONE = 'Asia/Irkutsk'
ALLOWED_HOSTS = ['']


ADMINS = (
    ('Dmitry Gorkovenko', 'gorkovenko.dmitry@gmail.com'),
)
MANAGERS = ADMINS

SITE_ID = 1

LANGUAGE_CODE = 'ru'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOCALE_PATHS = (
    os.path.join(ROOT_PATH, 'locale'),
)

PYMORPHY_DICTS = {
    'ru': {'dir': os.path.join(ROOT_PATH, 'pymorphy/ru')},
}

ADMIN_MEDIA_PREFIX = '/static/admin/'
ADMIN_MEDIA_ROOT = os.path.join(ROOT_PATH, 'static/admin')
MEDIA_ROOT = os.path.join(ROOT_PATH, 'media')
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.abspath(os.path.join(ROOT_PATH, 'static/'))
STATIC_URL = '/static/'

ROOT_URLCONF = 'project.urls'
WSGI_APPLICATION = 'project.wsgi.application'

STATICFILES_DIRS = (
    #os.path.abspath(os.path.join(ROOT_PATH, 'static')),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)



SECRET_KEY = '@2s4vz5mzve(s_ajmsd7q*iwr=dui95#e!7la-9cjn!k(yrk*q'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': False,
        'DIRS': [
            (os.path.join(ROOT_PATH, 'templates')),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': (
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'admin_tools.template_loaders.Loader',
            ),
            'context_processors': (
                'django.contrib.auth.context_processors.auth',
                'django.core.context_processors.i18n',
                'django.core.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.media',
                'django.core.context_processors.static',

                'apps.utils.context_processors.custom_processor',
                'apps.siteblocks.context_processors.settings',
                'apps.pages.context_processors.meta',
            )
        }
    },
]

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'pagination.middleware.PaginationMiddleware',
    'apps.pages.middleware.PageFallbackMiddleware',
    'apps.shorter.middleware.ShorterFallbackMiddleware',
)

CORS_ORIGIN_ALLOW_ALL = True

INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'pytils',
    'pymorphy',
    'sorl.thumbnail',
    'mptt',
    'salmonella',
    'pagination',
    'widget_tweaks',
    'import_export',
    'corsheaders',

    'apps.utils',
    'apps.pages',
    'apps.siteblocks',
    'apps.shorter',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


ADMIN_TOOLS_MENU = 'project.menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'project.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'project.dashboard.CustomAppIndexDashboard'

from project.local_settings import *
