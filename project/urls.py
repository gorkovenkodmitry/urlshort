from django.views.static import serve
from django.conf.urls import include, url
from django.contrib import admin

from apps.views import IndexView
from project import settings
from apps.utils.urls import urlpatterns as utils_urls


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^robots\.txt', serve, {'path': '/templates/robots.txt', 'document_root': settings.ROOT_PATH}),

    url(r'^shorter-api/', include('apps.shorter.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/salmonella/', include('salmonella.urls')),

    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
]
urlpatterns += utils_urls
