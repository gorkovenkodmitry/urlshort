#coding: utf-8
import os
import polib
from project import settings
from project.settings import ROOT_PATH

if hasattr(settings, 'LANGUAGES'):
    for x in settings.LANGUAGES:
        print x
        if x[0] == 'zh-cn':
            po = polib.pofile(u'locale/zh_CN/LC_MESSAGES/django.po')
            po.save_as_mofile(u'locale/zh_CN/LC_MESSAGES/django.mo')
        else:
            po = polib.pofile(u'locale/%s/LC_MESSAGES/django.po' % x[0])
            po.save_as_mofile(u'locale/%s/LC_MESSAGES/django.mo' % x[0])
else:
    APPS_PATH = os.path.join(ROOT_PATH, 'apps')
    for app in os.listdir(APPS_PATH):
        app_path = os.path.join(APPS_PATH, app)
        if os.path.isdir(app_path):
            if os.path.exists(os.path.join(app_path, 'locale/ru/LC_MESSAGES/django.po')):
                po = polib.pofile(os.path.join(app_path, 'locale/ru/LC_MESSAGES/django.po'))
                po.save_as_mofile(os.path.join(app_path, 'locale/ru/LC_MESSAGES/django.mo'))
    po = polib.pofile(u'locale/ru/LC_MESSAGES/django.po')
    po.save_as_mofile(u'locale/ru/LC_MESSAGES/django.mo')

print u'compile success'